//COPYRIGHT JAN
//Requirements
var Botkit = require("botkit");
var moment = require("moment");

var controller = Botkit.facebookbot({
  access_token: process.env.FB_ACCESS_TOKEN,
  verify_token: process.env.FB_VERIFY_TOKEN
});

var mysql = require("mysql");
var bot = controller.spawn();

//SQL Connection
var con = mysql.createConnection({
  host: "mcwellness-production-cluster.cluster-c08yqdgbyqct.eu-central-1.rds.amazonaws.com",
  user: "dbuser0002",
  password: "m4c#w3N3#st@9i7n9",
  database: "test_booking_system_3"
});
con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

//Customer struct
function Customer() {
  this.id = "";
  this.mail = "";
  this.booking_time = "";
  this.booking_date = "";
  this.booking_suite = "";
  this.outlet = "";
  this.booking_duration = 0;
  this.lastIntent = null;
  this.block_message = false;
  this.waiting_for_input = false;
  this.waiting_for_complaint = false;
  this.change_data = false;
  this.timeframe = "";
  this.mintime = "";
  this.maxtime = "";
  this.alt_hours = 0;
  this.alt_time = "";
  this.permanent_block = false;
  this.booking_number = 0;
  this.complaint_wait_1 = false;
  this.waiting_for_anliegen=false;
  this.wait_for_mail = false;
}
//Array of customers
var customers = new Array();

//List of outlets
function Outlet(){
    this.name = "";
    this.id = "";
}
//Array of outlets
var outlets = new Array();

//On Message received:
controller.hears("(.*)", "message_received", function (bot, message) {
    console.log(message.watsonData);
    //Customer id for this message session
    var id = 0;
    //First find the customer
    var customer_found = false;
    for(i=0;i<customers.length;i++){
        if(customers[i].id==message.watsonData.context.conversation_id)
        {
            id=i;
            customer_found=true;
            break;
        }
    }
    //If customer was not found
    if(!customer_found){
        //Create a new customer
        var c = new Customer();
        c.id = message.watsonData.context.conversation_id;
        customers.push(c);
        id = customers.length-1;
        //generate option buttons for first contact
        var buttons = [
            {content_type:"text", payload:"Ich habe eine generelle Frage", title:"Generelle Fragen"},
            {content_type:"text", payload:"Ich möchte Informationen zu meiner Buchung", title:"Buchungsinfos"},
            {content_type:"text", payload:"Ich möchte buchen", title:"Buchen"},
            {content_type:"text", payload:"Ich möchte mich beschweren", title:"Beschwerde"}
        ];
        //send the buttons with a delay, so that the greeting comes before the buttons
        setTimeout(function(){sendButtons(message,"Ich bin Berta, der Chatbot von McWellness. Wie kann ich dir helfen?",buttons);},2000);
    }
    //If the bot doesnt talk to this person anymore, return
    if(customers[id].permanent_block){
        return 0;
    }
    //Send usual watson reply to customer, if message send is not blocked
    if(!customers[id].block_message){
        console.log("REPLYING");
        bot.reply(message, message.watsonData.output.text.join("\n"));
    }
    //If sending is blocked, unblock it
    if(customers[id].block_message){
        customers[id].block_message=false;
    }
    //Get the intents
    if(message.watsonData.intents.length>0){
        //Get the intent with the biggest confidence score
        customers[id].lastIntent = message.watsonData.intents[0].intent;
    }
    //If we are waiting for a complaint intent
    if(customers[id].waiting_for_anliegen){
        bot.reply(message,"Danke! Ein Mitarbeiter wird sich bald bei dir melden");
        customers[id].permanent_block=true;
        customers[id].waiting_for_anliegen=false;
        return 0;
    }
    //if waiting for customer mail
    if(customers[id].wait_for_mail){
        customers[id].wait_for_mail=false;
        //check if mail was given
        var found = false;
        for(i=0;i<message.watsonData.entities.length;i++){
            if(message.watsonData.entities[i].entity == "mail"){
                //Mail was given
                customers[id].mail = message.watsonData.input.text;
                processBookingInfo(message,id);
                return 0;
            }
        }
    }
    //If the customer asked to change booking data
    switch(customers[id].lastIntent){
        case "BuchungsInfos":
            //If customer mail is known
            if(customers[id].mail.length>1){
                //Mail is known
                processBookingInfo(message,id);
                return 0;
            }else{
                //Mail is unknown
                customers[id].wait_for_mail = true;
                customers[id].block_message = true;
            }
        break;
        case "FilialeAndern":
            customers[id].outlet="";
        break;
        case "DatumAndern":
            customers[id].booking_date="";
        break;
        case "UhrzeitAndern":
            customers[id].booking_time="";
            customers[id].timeframe="";
        break;
        case "DauerAndern":
            customers[id].booking_duration=0;
        break;
        case "SuiteAndern":
            customers[id].booking_suite="";
        break;
        case "Abbrechen":
            customers[id].block_message=false;
            customers[id].waiting_for_input=false;
        break;
    }
    //if asked for suite difference, send the image
    if(customers[id].lastIntent == "SuiteUnterschiede" && message.watsonData.entities.length == 0){
        sendSuiteImage(message);
    }
    //If the last intent is suite_availibility, customer is probably in a booking process
    if(customers[id].lastIntent == "SuiteVerfugbarkeit" || customers[id].lastIntent == "MochteBuchen" || customers[id].waiting_for_input){
        //send data to completeBooking function
        customers[id].waiting_for_input=false;
        completeBooking(message,id);
    }
    //if the customer wants to make a complaint, watson asks for his booking number and we wait for his response
    if(customers[id].lastIntent == "Beschweren" || customers[id].waiting_for_complaint){
        customers[id].waiting_for_complaint=false;
        processComplaint(message,id);
    }
});



controller.on("facebook_postback", function (bot, message) {
    switch(message.payload){
        case "datenkorrekt":
        var id;
            for(i=0;i<customers.length;i++){
                if(message.watsonData.context.conversation_id == customers[i].id){
                    id = i;
                    break;
                }
            }
            var link = "https://buchen.mcwellness.me/?startzeit="+customers[id].booking_time.substring(0,2)+"&="+customers[id].booking_duration+"&personen=2&datum="+customers[id].booking_date+"&outlet_ids="+customers[id].outlet;
            bot.reply(message, "Bitte beende deine Buchung hier: "+ link);
        break;
        case "datennichtkorrekt":
            var id;
            for(i=0;i<customers.length;i++){
                if(customers[i].id == message.watsonData.context.conversation_id){
                    id = i;
                    break;
                }
            }
            customers[id].waiting_for_input=true;
            customers[id].block_message=true;
            askForDataChange(message,id);
        break;
        case "zeitanpassen":
            var id;
            for(i=0;i<customers.length;i++){
                if(customers[i].id == message.watsonData.context.conversation_id){
                    id = i;
                    break;
                }
            }
            customers[id].booking_time = customers[id].alt_time;
            completeBooking(message, id);
        break;
        case "daueranpassen":
            var id;
            for(i=0;i<customers.length;i++){
                if(customers[i].id == message.watsonData.context.conversation_id){
                    id = i;
                    break;
                }
            }
            customers[id].booking_duration = customers[id].alt_hours;
            completeBooking(message, id);
        break;
        case "nichtanpassen":
            bot.reply(message, "Alles klar :) Wenn du weiter buchen oder deine Daten ändern möchtest, schreibe einfach Ich möchte buchen");
        break;
    }
});

let processComplaint = function(message,id){
    //Waiting for booking number?
    if(customers[id].complaint_wait_1){
        customers[id].complaint_wait_1=false;
        var isnum = /^\d+$/.test(message.watsonData.input.text);
        if(isnum){
            customers[id].booking_number = message.watsonData.input.text;
        }else{
            if(message.watsonData.input.text.toLowerCase() == "abbrechen"){
                bot.reply(message,"Okay. Kann ich sonst noch etwas für dich tun??");
                return 0;
            }
            bot.reply(message,"Das ist keine gültige Buchungsnummer. Bitte verrate uns deine Buchungsnummer, schreibe 'abbrechen' zum abbrechen");
            customers[id].complaint_wait_1 = true;
            customers[id].block_message=true;
            customers[id].waiting_for_complaint=true;
            return 0;
        }
    }
    //First check if customer has set a booking number
    if(customers[id].booking_number == 0){
        //booking number is not set
        customers[id].block_message=true;
        customers[id].waiting_for_complaint=true;
        setTimeout(function(){bot.reply(message,"Bitte verrate uns deine Buchungsnummer für eine schnellere Bearbeitung.");},2000);
        customers[id].complaint_wait_1=true;
    }else{
        //booking number is set
        bot.reply(message,"Okay, bite schildere dein Anliegen :-)");
        customers[id].waiting_for_anliegen=true;
        customers[id].block_message=true;
    }
};

let completeBooking = function(message,id){
    //Check if customer gave new entities to us
    //only one entity at a time is accepted, because watson sometimes generates false-entities
    if(message.watsonData.entities.length==1){
        switch(message.watsonData.entities[0].entity){
            case "Standorte":
                //search the outlet id
                for(i=0;i<outlets.length;i++){
                    if(outlets[i].name == message.watsonData.entities[0].value){
                        //outlet found
                        customers[id].outlet = outlets[i].id;
                        break;
                    }
                }
            break;
            case "Duration":
                //We need to add 2 hours to the time, because of the german timezone
                customers[id].booking_duration = message.watsonData.entities[0].value;
            break;
            case "sys-time":
                customers[id].booking_time = message.watsonData.entities[0].value;
            break;
            case "sys-date":
                //if he says today we need to add 2 hrs
                var date = new Date();
                var c = moment(message.watsonData.entities[0].value, 'YYYY-MM-DD').add(2,'hours');
                if(c.diff(moment(date.getFullYear + "-" + (date.getMonth+1) + "-" + date.getDate, "YYYY-MM-DD"))<0){
                    bot.reply(message, "Das Datum liegt in der Vergangenheit. Bitte wähle ein neues.");
                    customers[id].block_message=true;
                    customers[id].waiting_for_input=true;
                    return 0;
                }
                customers[id].booking_date = c.format('YYYY-MM-DD');
            break;
            case "SuiteTypes":
                customers[id].booking_suite = message.watsonData.entities[0].value;
            break;
        }
    }
    //Watson known error: it is generating two entities for Duration
    if(message.watsonData.entities.length==2){
        message.watsonData.entities.forEach(function(element){
            if(element.entity=="Duration"){
                customers[id].booking_duration = element.value;
            }
            if(element.entity=="Timeframe"){
                customers[id].timeframe = element.value;
            }
        })
    }
    //if there is no entity, customer entered something else
    if(message.watsonData.entities.length==0 && customers[id].block_message && !customers[id].lastIntent=="MochteBuchen" && !customers[id].lastIntent=="SuiteVerfugbarkeit"){
        console.log("cancelling");
        bot.reply(message,message.watsonData.output.text.join("\n"));
        return 0;
    }
    //Now we are checking which information is given and which is missing
    //Starting with booking duration
    if(customers[id].booking_duration == 0){
        console.log("asking for duration");
        //Booking duration is not set, ask the customer how long he wants to stay. Block the next auto-answer
        askForDuration(message);
        customers[id].block_message=true;
        customers[id].waiting_for_input=true;
    }
    else{
        //Booking duration is set
        //Check if outlet is given
        if(customers[id].outlet == ""){
            console.log("asking for outlet");
            //Outlet not set, ask for outlet
            loadOutlets(message).then(function(result){
                console.log("loaded outlets, found " + outlets.length);
                var buttons = new Array();
                for(i=0;i<outlets.length;i++){
                    buttons.push({content_type:"text",title: outlets[i].name, payload: outlets[i].name});
                }
                sendButtons(message,"In welche Filiale willst du gehen?", buttons);
                console.log("sent buttons");
                customers[id].waiting_for_input=true;
                customers[id].block_message=true;
            })
        }else{
            //Outlet set
            //Now check for booking date
            if(customers[id].booking_date==""){
                //Booking date is not set. Ask for it and block next auto-answer.
                askForDate(message);
                customers[id].block_message=true;
                customers[id].waiting_for_input=true;
            }
            else{
                //Booking date is set
                //Now check for booking timeframe
                if(customers[id].timeframe==""){
                    //Booking timeframe is not set
                    askForTimeframe(message);
                    customers[id].block_message=true;
                    customers[id].waiting_for_input=true;
                }
                else{
                    //Booking timeframe is set
                    //Check for exact booking time
                    if(customers[id].booking_time==""){
                        //Booking time not set
                        askForTime(message,id);
                        customers[id].block_message=true;
                        customers[id].waiting_for_input=true;
                    }else{
                        //Booking time is set
                        //Check if suite is selected
                        if(customers[id].booking_suite==""){
                            //Suite not set
                            //Check what suite types are available at +- 30 minutes
                            checkAvailableSuiteTypes(customers[id].booking_time, id).then(function(result){
                                if(result==null){
                                    getAlternativeSlots(message,id);
                                    return 0;
                                }
                                //Generate quick-reply buttons
                                var buttons = new Array();
                                for(i=0;i<result.length;i++){
                                    buttons.push({content_type: "text", title: result[i], payload: result[i]});
                                }
                                //Send buttons to messenger
                                sendButtons(message,"Folgende Suite-Kategorien sind verfügbar. Welche möchtest du buchen?",buttons);
                                customers[id].block_message=true;
                                customers[id].waiting_for_input=true;
                            })
                        }else{
                            //Suite type is set
                            //Check if there is a booking slot available within +- 30 minutes of given time
                            checkForSlot(customers[id].booking_time, id).then(function(result){
                                if(result==null){
                                    //no suite available
                                    //get alternative suites, if available
                                    getAlternativeSlots(message,id);
                                    return 0;
                                }
                                console.log(result);
                                bot.reply(message,"Zusammenfassung: \n Datum: " + customers[id].booking_date + " \n Uhrzeit: " + customers[id].booking_time + " \n Filiale " + customers[id].outlet + " \n Suite: " + customers[id].booking_suite + " \n Stunden: " + customers[id].booking_duration);
                                bot.reply(message,"Um " + result.available_time + " ist noch eine Suite für dich frei ^_^");
                                askForReview(message,id);
                            })
                            //End check slot section
                        }
                    }
                }
            }
        }
    }
    console.log(customers[id]);
};

let getAlternativeSlots = function(message,id){
    //First, check if there is a slot available with the same booking time and a different duration
    var query;
    query = 'SELECT hours_available FROM test_booking_system_3.available_booking AS ab WHERE hours_available<='+ customers[id].booking_duration +' AND available_date="'+customers[id].booking_date+'" AND ( available_time BETWEEN "'+customers[id].mintime+'" AND "'+customers[id].maxtime+'" ) AND ab.outlet_id = '+customers[id].outlet + ' ORDER BY hours_available DESC LIMIT 1';
    sendQuery(query).then(function(result){
        if(result == null || result.length == 0){
            //no alternative duration found
            //now we search for alternative slots where duration is the same, but booking time is +-2 hrs instead of +-30 minutes
            var mintime = customers[id].mintime;
            var maxtime = customers[id].maxtime;
            mintime = moment(mintime, 'HH:mm:ss').subtract(90, 'minutes').format('HH:mm:ss');
            maxtime = moment(mintime, 'HH:mm:ss').add(90, 'minutes').format('HH:mm:ss');
            query = 'SELECT available_time FROM test_booking_system_3.available_booking AS ab WHERE hours_available<='+ customers[id].booking_duration +' AND available_date="'+customers[id].booking_date+'" AND ( available_time BETWEEN "'+mintime+'" AND "'+maxtime+'" ) AND ab.outlet_id = '+customers[id].outlet;
            sendQuery(query).then(function(r){
                if(r == null || r.length == 0){
                    //no alternative suite available
                    bot.reply(message,"Leider ist keine Suite mehr frei. Es sind auch keine Alternativtermine frei.");
                }else{
                    //alternative suites found
                    var mindiff;
                    var alt_time;
                    for(i=0;i<r.length;i++){
                        if(i==0){
                            alt_time = r.available_time;
                            mindiff = Math.abs(moment(customers[id].booking_time, 'HH:mm:ss').diff(moment(alt_time, 'HH:mm:ss'))); 
                        }else{
                            var diff = Math.abs(moment(customers[id].booking_time, 'HH:mm:ss').diff(moment(r.available_time, 'HH:mm:ss')));
                            if(diff < mindiff){
                                mindiff = diff;
                                alt_time = r.available_time;
                            }
                        }
                    }
                    //the time saved in alt_time is the closest available time
                    customers[id].alt_time = alt_time;
                    askForAltTime(message,id);
                }
            })
        }else{
            //alternative duration found
            var alt_hours = result[0].hours_available;
            customers[id].alt_hours = alt_hours;
            askForAltHours(message,id);
        }
    })
};

let sendButtons = function(message,text,buttons){
    console.log("hello! send buttons here");
    console.log(buttons);
    bot.reply(message, {
            text: text,
            quick_replies: buttons
        })
}

let sendSuiteImage = function(message){
    var attachment = {
        "type":"image",
        "payload":{
            "url":"https://i.imgur.com/HTyAjqo.png",
            "is_reusable": true
        }
    };
    controller.api.attachment_upload.upload(attachment, function (err, attachmentId) {
        if(err) {
            // Error
        } else {
            var image = {
                "attachment":{
                    "type":"image",
                    "payload": {
                        "attachment_id": attachmentId
                    }
                }
            };
            setTimeout(function(){bot.reply(message, image);},2000);
        }
    });
};

let askForDataChange = function(message,id){
    var buttons = [
        {content_type:"text", title:"Filiale", payload:"Ich möchte die Filiale ändern"},
        {content_type:"text", title:"Datum", payload:"Ich möchte das Datum ändern"},
        {content_type:"text", title:"Uhrzeit", payload:"Ich möchte die Uhrzeit ändern"},
        {content_type:"text", title:"Dauer", payload:"Ich möchte die Dauer ändern"},
        {content_type:"text", title:"Suitetyp", payload:"Ich möchte die Suite ändern"}
    ]
    sendButtons(message,"Welche Daten möchtest du ändern?",buttons);
};

let askForAltTime = function(message,id){
    var alt_time = customers[id].alt_time;
    var attachment = {
        'type':'template',
        'payload':{
            'template_type':'generic',
            'elements':[
                {
                    'title':'Buchungszeitpunkt anpassen?',
                    'buttons':[
                        {
                        'type':'postback',
                        'title':'Ja',
                        'payload':'zeitanpassen'
                        },
                        {
                        'type':'postback',
                        'title':'Nein',
                        'payload':'nichtanpassen'
                        }
                    ]
                },
            ]
        }
    };

    bot.reply(message, "Leider haben wir zu deinen Daten keine Suite mehr frei. Wir können dir jedoch noch eine Suite um " + alt_time + " Uhr anbieten.");
    bot.reply(message, {
        attachment: attachment,
    });
};

let processBookingInfo = function(message,id){
    //search for latest booking
    var query = 'SELECT * FROM test_booking_system_3.sales_booking WHERE customer_email="'+customers[id].mail+'" ORDER BY booking_date DESC LIMIT 1';
    sendQuery(query).then(function(result){
        //result empty?
        if(result == null || result.length==0){
            bot.reply(message,"Ich habe keine Buchung gefunden :(");
        }else{
            var b_datum = result[0].booking_date;
            var b_zeit = result[0].booking_time;
            var b_moment = moment(b_datum + "-" +b_zeit, 'YYYY-MM-DD-HH-mm-ss');
            var b_outlet = result[0].outlet_name;
            bot.reply(message, "Deine Buchung ist am " + b_moment.format("DD.MM.YYYY") + " um " + b_moment.format("HH:mm") + " in " + b_outlet);
        }
    })
};

let askForAltHours = function(message,id){
    var alt_hours = customers[id].alt_hours;
    var attachment = {
        'type':'template',
        'payload':{
            'template_type':'generic',
            'elements':[
                {
                    'title':'Buchungsdauer anpassen?',
                    'buttons':[
                        {
                        'type':'postback',
                        'title':'Ja',
                        'payload':'daueranpassen'
                        },
                        {
                        'type':'postback',
                        'title':'Nein',
                        'payload':'nichtanpassen'
                        }
                    ]
                },
            ]
        }
    };

    bot.reply(message, "Leider haben wir zu deinen Daten keine Suite mehr frei. Wir können dir jedoch noch eine Suite für " + alt_hours + " Stunden anbieten.");
    bot.reply(message, {
        attachment: attachment,
    });
};

let askForReview = function(message,id){
     var attachment = {
        'type':'template',
        'payload':{
            'template_type':'generic',
            'elements':[
                {
                    'title':'Sind die Buchungsdaten korrekt?',
                    'buttons':[
                        {
                        'type':'postback',
                        'title':'Ja',
                        'payload':'datenkorrekt'
                        },
                        {
                        'type':'postback',
                        'title':'Nein, ändern',
                        'payload':'datennichtkorrekt'
                        }
                    ]
                },
            ]
        }
    };

    bot.reply(message, {
        attachment: attachment,
    });
};

let loadOutlets = function(message){
    return new Promise(function(resolve,reject){
        if(outlets.length == 0){
            //outlets have not yet been loaded into the program. do it
            var query = 'SELECT outlet_id, outlet_name FROM test_booking_system_3.outlet WHERE is_active=1';
            sendQuery(query).then(function(result){
                console.log("result length:");
                console.log(result.length);
                if(result.length==0 || result==null){
                    //Error or no outlet found
                    bot.reply(message, "Es ist ein Fehler beim Laden der Filialen aufgetreten");
                }else{
                    //outlets found
                    //push to outlets array
                    for(i=0;i<result.length;i++){
                        var o = new Outlet();
                        o.id = result[i].outlet_id;
                        o.name = result[i].outlet_name;
                        outlets.push(o);
                    }
                    //Pushing done
                }
                resolve();
            })
        }else{
            resolve();
        }

    })
};

let checkAvailableSuiteTypes = function(time,id){
    return new Promise(function(resolve,reject){
        //time frame in which we search for slots: +- 30 minutes
        var minTime = moment(time, 'HH:mm:ss').subtract(30, 'minutes').format('HH:mm:ss');
        var maxTime = moment(time, 'HH:mm:ss').add(30, 'minutes').format('HH:mm:ss');
        customers[id].mintime = minTime;
        customers[id].maxtime = maxTime;
        //SQL Query
        var query = 'SELECT DISTINCT display_name FROM test_booking_system_3.available_booking AS ab, test_booking_system_3.suite AS su, test_booking_system_3.suite_type AS st WHERE hours_available>='+customers[id].booking_duration+' AND available_date="'+customers[id].booking_date+'" AND ( available_time BETWEEN "'+minTime+'" AND "'+maxTime+'" ) AND ab.outlet_id = '+customers[id].outlet+' AND ab.suite_id = su.suite_id AND su.suite_type_id = st.suite_type_id';
        sendQuery(query).then(function(result){
            if(result.length==0){
                resolve(null);
            }else{
                var available_suites = new Array();
                for(i=0;i<result.length;i++){
                    available_suites.push(result[i].display_name);
                }
                resolve(available_suites);
            }
        })
    })
}

let askForDuration = function(message){
    setTimeout(function(){
        bot.reply(message, {
            text: "Wie lange möchtest du denn bleiben?",
            quick_replies: [{
                content_type: "text",
                title: "1 Stunde",
                payload: "1std"
            },
            {
                content_type: "text",
                title: "2 Stunden",
                payload: "2std"
            },
            {
                content_type: "text",
                title: "3 Stunden",
                payload: "3std"
            },
            {
                content_type: "text",
                title: "4 Stunden",
                payload: "4std"
            }
            ]
        })
    },3000);
};

let askForDate = function(message){
    setTimeout(function(){
        bot.reply(message, "An welchem Tag möchtest du kommen?");
    },3000);
};

let askForTimeframe = function(message){
    setTimeout(function(){
        bot.reply(message, {
            text: "Wann möchtest du kommen?",
            quick_replies: [{
                content_type: "text",
                title: "Morgens",
                payload: "Morgens"
            },
            {
                content_type: "text",
                title: "Mittags",
                payload: "Mittags"
            },
            {
                content_type: "text",
                title: "Nachmittags",
                payload: "Nachmittags"
            },
            {
                content_type: "text",
                title: "Abends",
                payload: "Abends"
            }
            ]
        })
    },3000);
};

let askForTime = function(message,id){
    var buttons = new Array();
    switch(customers[id].timeframe.toLowerCase()){
        case "morgens":
            buttons.push({content_type:"text", title:"10 Uhr", payload:"10 Uhr"});
            buttons.push({content_type:"text", title:"11 Uhr", payload:"11 Uhr"});
        break;
        case "mittags":
            buttons.push({content_type:"text", title:"12 Uhr", payload:"12 Uhr"});
            buttons.push({content_type:"text", title:"13 Uhr", payload:"13 Uhr"});
        break;
        case "nachmittags":
            buttons.push({content_type:"text", title:"14 Uhr", payload:"14 Uhr"});
            buttons.push({content_type:"text", title:"15 Uhr", payload:"15 Uhr"});
            buttons.push({content_type:"text", title:"16 Uhr", payload:"16 Uhr"});
            buttons.push({content_type:"text", title:"17 Uhr", payload:"17 Uhr"});
        break;
        case "abends":
            buttons.push({content_type:"text", title:"18 Uhr", payload:"18 Uhr"});
            buttons.push({content_type:"text", title:"19 Uhr", payload:"19 Uhr"});
            buttons.push({content_type:"text", title:"20 Uhr", payload:"20 Uhr"});
            buttons.push({content_type:"text", title:"21 Uhr", payload:"21 Uhr"});
            buttons.push({content_type:"text", title:"22 Uhr", payload:"22 Uhr"});
        break;
    }
    bot.reply(message, {
            text: "Um wie viel Uhr genau? (+- 30 Minuten)?",
            quick_replies: buttons
        })
};

let checkForSlot = function(time,id){
    //Using promises to wait for the queries to finish
    return new Promise(function(resolve,reject){
        //time frame in which we search for slots: +- 30 minutes
        var minTime = moment(time, 'HH:mm:ss').subtract(30, 'minutes').format('HH:mm:ss');
        var maxTime = moment(time, 'HH:mm:ss').add(30, 'minutes').format('HH:mm:ss');
        //SQL Query
        var query = 'SELECT available_date, available_time, ab.suite_id, su.suite_type_id, display_name FROM test_booking_system_3.available_booking AS ab, test_booking_system_3.suite AS su, test_booking_system_3.suite_type AS st WHERE st.display_name="'+customers[id].booking_suite+'" AND hours_available>='+customers[id].booking_duration+' AND available_date="'+customers[id].booking_date+'" AND ( available_time BETWEEN "'+minTime+'" AND "'+maxTime+'" ) AND ab.outlet_id = 3 AND ab.suite_id = su.suite_id AND su.suite_type_id = st.suite_type_id';
        console.log(query);
        //Send the query to the sending Promise
        sendQuery(query).then(function(result){
            //if there is no slot found
            if(result.length==0){
                resolve(null);
            }
            //if there are slots found
            var mindiff = 0;
            var closest;
            //Find the booking slot which is closest to desired time
            for(i=0;i<result.length;i++){
                if(i==0){
                    closest = result[i];
                    mindiff = Math.abs(moment(closest.available_time, 'HH:mm:ss').diff(moment(time, 'HH:mm:ss')));
                }else{
                    if(Math.abs(moment(result[i].available_time, 'HH:mm:ss').diff(moment(time, 'HH:mm:ss'))) < mindiff){
                        closest = result[i];
                        mindiff = Math.abs(moment(closest.available_time, 'HH:mm:ss').diff(moment(time, 'HH:mm:ss')));
                    }
                }
            }
            //Once query is done with all entries, resolve.
            resolve(closest);
        })
    })
};

let sendQuery = function(query){
    return new Promise(function (resolve, reject) {
        con.query(query, function(err,rows){
            console.log(query);
            console.log("resolving " + rows.length + " rows");
            console.log(rows);
            resolve(rows);
        })
    })
}
           

module.exports.controller = controller;
module.exports.bot = bot;